package ca.datavic.overtime.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface OvertimeItemDAO {
    @Insert
    void insert(OvertimeItem oti);

    @Query("DELETE FROM overtime_item")
    void deleteAll();

    @Query("SELECT * from overtime_item ORDER BY event_date DESC")
    List<OvertimeItem> getAllItems();

    @Query("SELECT * from overtime_item WHERE event = :eventNumber")
    OvertimeItem getItemsByEvent(String eventNumber);
}
