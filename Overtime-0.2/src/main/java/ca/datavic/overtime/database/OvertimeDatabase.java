package ca.datavic.overtime.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {OvertimeItem.class}, version = 2)
public abstract class OvertimeDatabase extends RoomDatabase {
    public abstract OvertimeItemDAO otiDao();
    private static OvertimeDatabase INSTANCE;

    public static OvertimeDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (OvertimeDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            OvertimeDatabase.class, "overtime_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
