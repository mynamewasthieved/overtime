package ca.datavic.overtime.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.util.Log;

import ca.datavic.overtime.utilities.OvertimeConversions;

@Entity(tableName = "overtime_item")
public class OvertimeItem implements android.os.Parcelable {

    public OvertimeItem(String employeeNumber,String eventNumber,long eventDate,String shift, long startTime,long endTime,
                        String explanation, String type,String newCall,String percentCash,String comments ){
        this.eventNumber = eventNumber;
        this.employeeNumber = employeeNumber;
        this.eventDate = eventDate;
        this.shift = shift;
        this.startTime = startTime;
        this.endTime = endTime;
        this.explanation = explanation;
        this.type = type;
        this.percentCash = percentCash;
        this.newCall = newCall;
        this.comments = comments;
    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "event")
    private String eventNumber;

    @ColumnInfo(name = "employee_number")
    private String employeeNumber;

    @ColumnInfo(name = "event_date")
    private long eventDate;

    @ColumnInfo(name = "shift")
    private String shift;

    @ColumnInfo(name = "start_time")
    private long startTime;

    @ColumnInfo(name = "end_time")
    private long endTime;

    @ColumnInfo(name = "explanation")
    private String explanation;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "new_call")
    private String newCall;


    @ColumnInfo(name = "percent_cash")
    private String percentCash;

    @ColumnInfo(name = "comments")
    private String comments;

    protected OvertimeItem(Parcel in) {
        eventNumber = in.readString();
        employeeNumber = in.readString();
        eventDate = in.readLong();
        shift = in.readString();
        startTime = in.readLong();
        endTime = in.readLong();
        explanation = in.readString();
        type = in.readString();
        newCall = in.readString();
        percentCash = in.readString();
        comments = in.readString();
    }

    public static final Creator<OvertimeItem> CREATOR = new Creator<OvertimeItem>() {
        @Override
        public OvertimeItem createFromParcel(Parcel in) {
            return new OvertimeItem(in);
        }

        @Override
        public OvertimeItem[] newArray(int size) {
            return new OvertimeItem[size];
        }
    };

    @NonNull
    public String getEventNumber() {
        return eventNumber;
    }

    public void setEventNumber(@NonNull String eventNumber) {
        this.eventNumber = eventNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public long getEventDate() {
        return eventDate;
    }

    public void setEventDate(long eventDate) {
        this.eventDate = eventDate;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNewCall() {
        return newCall;
    }

    public void setNewCall(String newCall) {
        this.newCall = newCall;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPercentCash() {
        return percentCash;
    }

    public void setPercentCash(String percentCash) {
        this.percentCash = percentCash;
    }

    public double getTotalTime(){
        return OvertimeConversions.durationAsRoundedDecimal(getStartTime(),getEndTime());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventNumber);
        dest.writeString(employeeNumber);
        dest.writeLong(eventDate);
        dest.writeString(shift);
        dest.writeLong(startTime);
        dest.writeLong(endTime);
        dest.writeString(explanation);
        dest.writeString(type);
        dest.writeString(newCall);
        dest.writeString(percentCash);
        dest.writeString(comments);
    }
}
