package ca.datavic.overtime.excel;

import android.app.Activity;
import android.util.Log;

import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ca.datavic.overtime.R;
import ca.datavic.overtime.database.OvertimeItem;
import ca.datavic.overtime.utilities.OvertimeDateFormatter;

public class ExcelHandler {
    private static String TAG = "**FILTER** " + "ExcelHandler";
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private File workingCopy;
    private ArrayList<ExcelEditCell> mEdits = new ArrayList<>();

    public ExcelHandler(Activity mActivity,OvertimeItem mOti){
        addEdit(new ExcelEditCell("J5", OvertimeDateFormatter.dateFormatForExcel(new Date(mOti.getEventDate()))));
        addEdit(new ExcelEditCell("J6", mOti.getEmployeeNumber()));
        addEdit(new ExcelEditCell("J7", mActivity.getString(R.string.temp_user_first_name)));
        addEdit(new ExcelEditCell("J8", mActivity.getString(R.string.temp_user_last_name)));
        addEdit(new ExcelEditCell("A10", mOti.getShift()));
        addEdit(new ExcelEditCell("B10", mOti.getEventNumber()));
        addEdit(new ExcelEditCell("C10", mOti.getExplanation()));
        addEdit(new ExcelEditCell("E10", OvertimeDateFormatter.timeFormatForExcel(new Date(mOti.getStartTime()))));
        addEdit(new ExcelEditCell("F10", OvertimeDateFormatter.timeFormatForExcel(new Date(mOti.getEndTime()))));
        addEdit(new ExcelEditCell("D6", mActivity.getString(R.string.temp_user_home_station)));
        addEdit(new ExcelEditCell("D7", mActivity.getString(R.string.temp_user_status)));
        addEdit(new ExcelEditCell("I10", mOti.getNewCall()));
        addEdit(new ExcelEditCell("A38", mOti.getComments()));
        addEdit(new ExcelEditCell("K10", mOti.getPercentCash()));
        Log.d(TAG, "ExcelHandler: %cash=" + mOti.getPercentCash());
        addEdit(new ExcelEditCell("J10", mOti.getType()));
        Log.d(TAG, "ExcelHandler: total edits = " + mEdits.size());
    }

    public void edit(File excelFile){
        workingCopy = excelFile;
        try {
            InputStream targetStream = new FileInputStream(excelFile);
            workbook = new XSSFWorkbook(targetStream);
            sheet = workbook.getSheetAt(0);
            save();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "edit: ERROR-FileNotFoundException");
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "edit: ERROR-IOException");
            e.printStackTrace();
        }
    }

    private void save(){
        for (ExcelEditCell edit : mEdits){
            editCell(edit.getCellRef(),edit.getValue());
        }

        OutputStream saveFile;
        try {
            saveFile = new FileOutputStream(workingCopy);
            workbook.write(saveFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addEdit(ExcelEditCell editCell){
        mEdits.add(editCell);
    }

    private void editCell(String reference, Object value){
        Log.d(TAG, "editCell: ref" + reference);
        CellReference ref = new CellReference(reference);
        Row r = sheet.getRow(ref.getRow());
        if (r != null) {
            Cell c = r.getCell(ref.getCol());
            if( value instanceof String) {
                c.setCellValue((String)value);
            } else if( value instanceof Calendar) {
                c.setCellValue((Calendar) value);
            } else if (value instanceof Date) {
                c.setCellValue((Date) value);
            } else if(value instanceof Integer){
                c.setCellValue((Integer)value);
            } else if(value instanceof Long){
                c.setCellValue((Long)value);
            }  else if(value instanceof Boolean){
                c.setCellValue((Boolean) value);
            }
        }
    }


}
