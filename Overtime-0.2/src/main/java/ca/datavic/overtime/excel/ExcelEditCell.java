package ca.datavic.overtime.excel;

import java.util.Calendar;
import java.util.Date;

public class ExcelEditCell {
    private String mCellRef;
    private Object mValue;

    ExcelEditCell(String reference, String value){
        mCellRef = reference;mValue = value;
    }
    public ExcelEditCell(String reference,Calendar value){
        mCellRef = reference;
        mValue = value;
    }
    public ExcelEditCell(String reference,Date value){
        mCellRef = reference;
        mValue = value;
    }

    public ExcelEditCell(String reference,long value){
        mCellRef = reference;
        mValue = value;
    }

    public ExcelEditCell(String reference,int value){
        mCellRef = reference;
        mValue = value;
    }

    public ExcelEditCell(String reference,boolean value){
        mCellRef = reference;
        mValue = value;
    }

    public String getCellRef() {
        return mCellRef;
    }

    public Object getValue(){
       return mValue;
    }

}
