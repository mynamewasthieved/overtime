package ca.datavic.overtime;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import ca.datavic.overtime.database.OvertimeDatabase;
import ca.datavic.overtime.frags.display.FragContentMain;
import ca.datavic.overtime.gmail.GmailAuth;
import ca.datavic.overtime.utilities.OvertimeFileHandler;
import ca.datavic.overtime.utilities.TestData;

public class MainActivity extends AppCompatActivity {
    private static String TAG = "LOGGER:" + "MainActivity";
    public static final int RC_SIGN_IN = 9999;
    public static OvertimeDatabase db;
    FragmentManager fragmentManager;
    public static GoogleSignInAccount mAccount;
    GoogleSignInClient mGoogleSignInClient;
    Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = OvertimeDatabase.getDatabase(MainActivity.this);
        invalidateOptionsMenu();
        mAccount = GmailAuth.getGmailAccount(MainActivity.this);
        mGoogleSignInClient = GmailAuth.getGoogleSingInClient(MainActivity.this);
        fragmentManager = getFragmentManager();
        showMainContent();
        setupFiles();
        TestData.fillOti(MainActivity.this);
    }

    private void setupFiles(){
        OvertimeFileHandler fh = new OvertimeFileHandler(MainActivity.this);
        fh.filesSetup();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        this.mMenu = menu;
        updateMenu();
        return true;
    }

    private void showMainContent(){
        if(mAccount != null){
            FragContentMain mainFragment = new FragContentMain();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container_main, mainFragment,"MAIN");
            fragmentTransaction.addToBackStack("MAIN");
            fragmentTransaction.commit();
        } else {
            if(fragmentManager.getBackStackEntryCount() > 0){
                fragmentManager.popBackStack();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_account:
                if(item.getTitle().equals(getString(R.string.menu_item_sign_in))){
                    signIn();
                } else {
                    mGoogleSignInClient.signOut();
                    signIn();
                }
                return true;
            case R.id.menu_item_sign_out:
                mGoogleSignInClient.signOut();
                mAccount = null;
                updateMenu();
                showMainContent();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            mAccount = completedTask.getResult(ApiException.class);
            updateMenu();
            showMainContent();
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateMenu();
            showMainContent();
        }
    }

    private void updateMenu(){
        if(mAccount != null){
            mMenu.findItem(R.id.menu_item_account).setTitle(mAccount.getEmail());
        } else {
            mMenu.findItem(R.id.menu_item_account).setTitle(R.string.menu_item_sign_in);
        }
    }
}
