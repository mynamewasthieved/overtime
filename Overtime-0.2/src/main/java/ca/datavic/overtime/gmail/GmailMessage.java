package ca.datavic.overtime.gmail;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.model.Message;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import ca.datavic.overtime.R;

public class GmailMessage {
    private String mTo,mFrom,mSubject,mBody;
    private ArrayList<BodyPart> mParts = new ArrayList<>();
    private Message mMessage;
    public void setMessage(Message message){
        mMessage = message;
    }
    public Message getMessage(){
        return mMessage;
    }

    public GmailMessage(Context context){
        mTo = context.getString(R.string.overtime_email_to);
        mFrom = "me";
        mSubject = context.getString(R.string.overtime_email_subject);

        addBodyPartText(context.getString(R.string.overtime_email_body));
        mBody = context.getString(R.string.overtime_email_body);

        new BuildMessageTask().execute();

    }

    public void addBodyPartText(String text){
        BodyPart messageBodyPart = new MimeBodyPart();
        try{
            messageBodyPart.setText(text);
            mParts.add(messageBodyPart);
        } catch (MessagingException me){
            me.printStackTrace();
        }
    }

    public void addAttachment(File file){
        BodyPart messageBodyPart = new MimeBodyPart();
        try {
            if(file != null){
                DataSource source = new FileDataSource(file);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(file.getName());
            } else {
                messageBodyPart.setText("Attachment Failed");
            }

            mParts.add(messageBodyPart);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class BuildMessageTask extends AsyncTask<Object, Integer, Message> {
        private String TAG = "LOGGER:" + "GmailMessage";
        @Override
        protected Message doInBackground(Object... objects) {
            Properties props = new Properties();
            Session session = Session.getInstance(props, null);

            MimeMessage mm =  new MimeMessage(session);
            Multipart multipart = new MimeMultipart();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Message message = new Message();

            try{
                mm.setFrom(new InternetAddress(mFrom));
                mm.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(mTo));
                mm.setSubject(mSubject);
                for (BodyPart part : mParts){
                    multipart.addBodyPart(part);
                }
                mm.setContent(multipart);
                mm.writeTo(baos);
                Log.d(TAG, "doInBackground: \n" + baos.toString());
                String encodedEmail = Base64.encodeBase64URLSafeString(baos.toByteArray());
                message.setRaw(encodedEmail);
            } catch (MessagingException | IOException me){
                me.printStackTrace();
            }
            return message;
        }
        @Override
        protected void onPostExecute(Message result) {
            setMessage(result);
        }
    }
}
