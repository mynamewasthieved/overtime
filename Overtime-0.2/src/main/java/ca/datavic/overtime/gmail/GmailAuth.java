package ca.datavic.overtime.gmail;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.GmailScopes;

import java.util.Arrays;

import ca.datavic.overtime.MainActivity;

public class GmailAuth {
    private static String TAG = "LOGGER:" + "GmailAuth";

    private static String[] SCOPES = { GmailScopes.MAIL_GOOGLE_COM,
            GmailScopes.GMAIL_SEND };

    public static final int REQUEST_ACCOUNT_PICKER = 3001;

    public static GoogleSignInAccount getGmailAccount(Activity activity){
        return GoogleSignIn.getLastSignedInAccount(activity);
    }
    public static GoogleSignInClient getGoogleSingInClient(Activity activity){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        return GoogleSignIn.getClient(activity, gso);
    }

    public static GoogleAccountCredential getCredentials(Activity activity){

        GoogleAccountCredential mCredential = GoogleAccountCredential.usingOAuth2(activity, Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        if(MainActivity.mAccount != null){
            mCredential.setSelectedAccount(new Account(MainActivity.mAccount.getEmail(),activity.getPackageName()));
        }
        return mCredential;
    }
}
