package ca.datavic.overtime.gmail;

import android.os.AsyncTask;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.model.Message;

import java.io.IOException;

public class GmailSender {
    private GmailMessage mGmailMessage;

    public GmailSender(GmailMessage gmailMessage){
        mGmailMessage = gmailMessage;
    }

    public void send(GoogleAccountCredential credential){
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        com.google.api.services.gmail.Gmail mService = new com.google.api.services.gmail.Gmail.Builder(
                transport, jsonFactory, credential)
                .setApplicationName("Overtime")
                .build();
        new SentTask().execute(mService,mGmailMessage);
    }


    private static class SentTask extends AsyncTask<Object,Integer,Message>{
        com.google.api.services.gmail.Gmail mService;
       GmailMessage mMessage;
        @Override
        protected Message doInBackground(Object... objects) {
            mService = (com.google.api.services.gmail.Gmail)objects[0];
            mMessage = (GmailMessage)objects[1];
            Message message;
            try {
                message = mService.users().messages().send("me", mMessage.getMessage()).execute();
                return message;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        @Override
        protected void onPostExecute(Message result) {

        }
    }

}
