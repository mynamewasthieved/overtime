package ca.datavic.overtime.frags.display;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import ca.datavic.overtime.R;
import ca.datavic.overtime.database.OvertimeItem;


public class PayPeriodAdapter extends ExpandableRecyclerViewAdapter<PayPeriodViewHolder, OvertimeItemViewHolder> {

    public PayPeriodAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public PayPeriodViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_pay_period, parent, false);
        return new PayPeriodViewHolder(view);
    }

    @Override
    public OvertimeItemViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_overtime_item, parent, false);
        return new OvertimeItemViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(OvertimeItemViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final OvertimeItem oti = ((PayPeriod) group).getItems().get(childIndex);
        holder.fillViews(oti);
        //holder.setEventNumber(oti.getEventNumber());
    }

    @Override
    public void onBindGroupViewHolder(PayPeriodViewHolder holder, int flatPosition, ExpandableGroup group) {
        final int otCountForPayPeriod = ((PayPeriod) group).getItems().size();
        holder.setPayPeriodTitle(group);
        holder.setCount(otCountForPayPeriod);
        double totalOt = 0.0;
        double totalEt = 0.0;
        for(OvertimeItem oti : ((PayPeriod) group).getItems()){
            if(oti.getType().equals("Shift O/T")){
                totalOt = totalOt + oti.getTotalTime();
            } else {
                totalEt = totalEt + oti.getTotalTime();
            }
        }
        holder.setPayPeriodTotalOt(totalOt);
        holder.setPayPeriodTotalExtended(totalEt);
    }
}
