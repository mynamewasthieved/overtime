package ca.datavic.overtime.frags.display;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.util.Date;

import ca.datavic.overtime.R;
import ca.datavic.overtime.database.OvertimeItem;
import ca.datavic.overtime.utilities.OvertimeDateFormatter;

public class OvertimeItemViewHolder extends ChildViewHolder {

    private TextView tvEventNumber,tvDate,tvStartTime,tvEndTime,tvTotalTime,tvType;

    public OvertimeItemViewHolder(View itemView){
        super(itemView);
        tvEventNumber = (TextView) itemView.findViewById(R.id.rvr_event_number);
        tvDate = itemView.findViewById(R.id.rvr_date);
        tvStartTime = itemView.findViewById(R.id.rvr_start_time);
        tvEndTime = itemView.findViewById(R.id.rvr_end_time);
        tvTotalTime = itemView.findViewById(R.id.rvr_total_time);
        tvType = itemView.findViewById(R.id.rvr_type);

    }

    public void fillViews(OvertimeItem oti){
        tvEventNumber.setText(oti.getEventNumber());
        tvDate.setText(OvertimeDateFormatter.dateFormatForUi(new Date(oti.getEventDate())));
        tvStartTime.setText(OvertimeDateFormatter.timeFromLong(oti.getStartTime()));
        tvEndTime.setText(OvertimeDateFormatter.timeFromLong(oti.getEndTime()));
        tvTotalTime.setText(String.valueOf(oti.getTotalTime()));
        tvType.setText(oti.getType());
    }

    //public void setEventNumber(String eventNumber) {
    //    tvEventNumber.setText(eventNumber);
    //}

}



