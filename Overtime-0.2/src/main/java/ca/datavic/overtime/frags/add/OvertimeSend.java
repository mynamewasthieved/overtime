package ca.datavic.overtime.frags.add;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import java.io.File;

import ca.datavic.overtime.database.OvertimeItem;
import ca.datavic.overtime.excel.ExcelHandler;
import ca.datavic.overtime.gmail.GmailAuth;
import ca.datavic.overtime.gmail.GmailMessage;
import ca.datavic.overtime.gmail.GmailSender;
import ca.datavic.overtime.utilities.OvertimeFileHandler;

import static ca.datavic.overtime.MainActivity.db;

public class OvertimeSend {
    private static String TAG = "LOGGER:" + "OvertimeSend";
    private Activity mActivity;
    private ProgressDialog sendProgressDialog;
    OvertimeSend(Activity activity){
        mActivity = activity;

        sendProgressDialog = new ProgressDialog(mActivity);
        sendProgressDialog.setTitle("Sending Mail");
        sendProgressDialog.setMessage("Your overtime is being sent.\n\n Please wait...\n\n");
        sendProgressDialog.setIndeterminate(true);
    }

    public void send(OvertimeItem oti){
        OvertimeFileHandler fileHandler = new OvertimeFileHandler(mActivity);
        File workingCopy = fileHandler.copy();
        Log.d(TAG, "OvertimeSend: " + workingCopy.getAbsoluteFile());
        new SentOvertimeTask().execute(mActivity, oti, workingCopy,sendProgressDialog);
    }
    private void showProgress(boolean shouldShow){
        if(shouldShow){
            sendProgressDialog.show();
        } else {
            sendProgressDialog.dismiss();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SentOvertimeTask extends AsyncTask<Object,Integer,String> {
        OvertimeItem tOti;
        File tWorkingCopy;
        @Override
        protected String doInBackground(Object... objects) {

            Activity tActivity = (Activity) objects[0];
            tOti = (OvertimeItem) objects[1];
            tWorkingCopy = (File) objects[2];

            db.otiDao().insert(tOti);
            Log.d(TAG, "run: item count = " + db.otiDao().getAllItems().size());

            ExcelHandler excelHandler = new ExcelHandler(tActivity,tOti);
            excelHandler.edit(tWorkingCopy);

            GmailMessage mGmailMessage= new GmailMessage(tActivity);
            mGmailMessage.addBodyPartText("This is additional text");
            mGmailMessage.addAttachment(tWorkingCopy);
            GmailSender gms = new GmailSender(mGmailMessage);
            GoogleAccountCredential cred = GmailAuth.getCredentials(tActivity);
            if(cred.getSelectedAccountName() == null){
                tActivity.startActivityForResult(
                        cred.newChooseAccountIntent(),GmailAuth.REQUEST_ACCOUNT_PICKER);
            } else {
                gms.send(GmailAuth.getCredentials(tActivity));
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            showProgress(false);
            super.onPostExecute(s);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
    }
}
