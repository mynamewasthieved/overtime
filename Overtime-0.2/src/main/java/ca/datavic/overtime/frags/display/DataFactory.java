package ca.datavic.overtime.frags.display;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import ca.datavic.overtime.database.OvertimeItem;
import ca.datavic.overtime.utilities.OvertimeDateFormatter;

import static ca.datavic.overtime.MainActivity.db;

public class DataFactory {
    private static String TAG = "LOGGER:" + "DataFactory";
    public static String ANCHOR_DATE = "05 Jan 2018";
    static ArrayList<OvertimeItem> mOtis = new ArrayList();
    public static List<PayPeriod> makePayPeriods() {


        mOtis = (ArrayList<OvertimeItem>) db.otiDao().getAllItems();
        Log.d(TAG, "makePayPeriods: motis size = " + mOtis.size());

        Calendar stop = Calendar.getInstance();
        stop.add(Calendar.DATE,14);
        Date stopDate = stop.getTime();

        Calendar anchorDate = Calendar.getInstance();
        anchorDate.setTime(OvertimeDateFormatter.dateFromUiString(ANCHOR_DATE));

        Date sDate = anchorDate.getTime();
        anchorDate.add(Calendar.DATE,14);
        Date eDate = anchorDate.getTime();

        ArrayList<PayPeriod> pp = new ArrayList<>();

        while(sDate.before(stopDate)){
            Log.d(TAG, "makePayPeriods: testing pp s=" + OvertimeDateFormatter.dateTimeForLog(sDate) +
                    " e=" + OvertimeDateFormatter.dateTimeForLog(eDate));

            ArrayList<OvertimeItem> tempFiller = new ArrayList<>();
            for(OvertimeItem oti : mOtis){
                if(testDate(sDate,eDate,new Date(oti.getEventDate()))){
                    Log.d(TAG, "makePayPeriods: yep");
                    tempFiller.add(oti);
                }
            }
            if(tempFiller.size() > 0){
                pp.add(new PayPeriod(OvertimeDateFormatter.getPayPeriodTitle(sDate,eDate), tempFiller));
                //PayPeriod p1 = new PayPeriod("Pay Period 1",pp1);
            }
            sDate = anchorDate.getTime();
            anchorDate.add(Calendar.DATE,14);
            eDate = anchorDate.getTime();
        }
        Collections.reverse(pp);
        return pp;
    }

    private static boolean testDate(Date sDate,Date eDate, Date value){
        return sDate.compareTo(value) * value.compareTo(eDate) >= 0;
    }

}
