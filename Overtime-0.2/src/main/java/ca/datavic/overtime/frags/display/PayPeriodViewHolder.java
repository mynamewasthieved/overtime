package ca.datavic.overtime.frags.display;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import ca.datavic.overtime.R;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class PayPeriodViewHolder extends GroupViewHolder {

    private TextView ppTitle,ppCount,ppTotalOt,ppTotalExtended;
    private ImageView arrow;

    public PayPeriodViewHolder(View itemView) {
        super(itemView);
        ppTitle = (TextView) itemView.findViewById(R.id.list_item_pay_period_title);
        ppCount = itemView.findViewById(R.id.tv_pay_period_total_items);
        ppTotalOt = itemView.findViewById(R.id.tv_total_overtime);
        ppTotalExtended = itemView.findViewById(R.id.tv_total_extended_tour);
        arrow = (ImageView) itemView.findViewById(R.id.list_item_pay_period_arrow);
    }

    public void setPayPeriodTitle(ExpandableGroup payPeriod) {
        if (payPeriod instanceof PayPeriod) {
            ppTitle.setText(payPeriod.getTitle());
        }
    }

    public void setPayPeriodTotalOt(double totalOt) {
        ppTotalOt.setText(String.valueOf(totalOt));
    }
    public void setPayPeriodTotalExtended(double totalExtended) {
        ppTotalExtended.setText(String.valueOf(totalExtended));
    }


    public void setCount(int count) {
        ppCount.setText(String.valueOf(count));
    }


    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }
}