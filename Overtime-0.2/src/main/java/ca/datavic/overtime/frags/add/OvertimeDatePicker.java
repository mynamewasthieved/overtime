package ca.datavic.overtime.frags.add;

import android.app.DialogFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.Date;

import ca.datavic.overtime.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OvertimeDatePicker.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OvertimeDatePicker#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OvertimeDatePicker extends DialogFragment {


    private OnDateSetListener mListener;

    public OvertimeDatePicker() {
        // Required empty public constructor
    }


    public static OvertimeDatePicker newInstance() {
        return new OvertimeDatePicker();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overtime_date_picker, container, false);

        MaterialCalendarView calendarView = view.findViewById(R.id.calendarView);
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                mListener.onDateSet(date.getDate());
                OvertimeDatePicker.this.dismiss();
            }
        });

        //getDialog().setTitle("Test");

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setOnDateSetListener(OnDateSetListener listener){
        mListener = listener;
    }

    public interface OnDateSetListener {
        void onDateSet(Date date);
    }
}
