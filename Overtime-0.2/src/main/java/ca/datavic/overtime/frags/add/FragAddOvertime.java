package ca.datavic.overtime.frags.add;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import net.steamcrafted.lineartimepicker.dialog.LinearTimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ca.datavic.overtime.R;
import ca.datavic.overtime.database.OvertimeItem;
import ca.datavic.overtime.utilities.OvertimeAddValidator;
import ca.datavic.overtime.utilities.OvertimeAddValidator.Valid;
import ca.datavic.overtime.utilities.OvertimeDateFormatter;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragAddOvertime extends Fragment implements View.OnClickListener
        , Spinner.OnItemSelectedListener {
    private static String TAG = "LOGGER:" + "FragAddOvertime";
    View mView;
    TextInputEditText etDate, etReason,etEventNumber,etShift,etStartTime,etEndTime,etComments;
    ImageView ivStart,ivEnd,ivDate;
    AppCompatButton btnSend,btnCancel;
    String mEmpStatus;
    String isNewCall;
    String mOvertimeType;
    Spinner spnStatus,spnNewCall,spnOtType;
    SeekBar seekCashCto;
    TextView tvCto,tvCash;
    int ctoCashValue;

    public FragAddOvertime() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_frag_add_overtime, container, false);

        initUi(mView);
        return mView;
    }

    private void initUi(View view){
        etDate = view.findViewById(R.id.ti_date);
        etReason = view.findViewById(R.id.ti_reason);
        etEventNumber = view.findViewById(R.id.ti_event_number);
        etShift = view.findViewById(R.id.ti_shift);
        etStartTime = view.findViewById(R.id.ti_start_time);
        etEndTime = view.findViewById(R.id.ti_end_time);
        etComments = view.findViewById(R.id.ti_comments);
        ivStart = view.findViewById(R.id.iv_start_time);
        ivEnd = view.findViewById(R.id.iv_end_time);
        ivDate = view.findViewById(R.id.iv_date_icon);
        btnSend = view.findViewById(R.id.btn_send);
        btnCancel = view.findViewById(R.id.btn_cancel);
        spnStatus = view.findViewById(R.id.spn_status);
        spnNewCall = view.findViewById(R.id.spn_new_call);
        seekCashCto = view.findViewById(R.id.seek_cto_cash);
        tvCash = view.findViewById(R.id.tv_percent_cash);
        tvCto = view.findViewById(R.id.tv_percent_cto);
        spnOtType = view.findViewById(R.id.spn_ot_type);

        //set defaults
        etDate.setText(OvertimeDateFormatter.dateFormatForUi(new Date()));
        etReason.setText(getString(R.string.form_default_reason));
        etShift.setText(getString(R.string.form_default_shift));
        etStartTime.setText(getString(R.string.form_default_overtime_start));

        Calendar c = Calendar.getInstance();
        //c.set(Calendar.HOUR_OF_DAY,3);
        //c.set(Calendar.MINUTE,5);

        String currentTime = OvertimeDateFormatter.getDisplayTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE));
        etEndTime.setText(currentTime);
        seekCashCto.setMax(100);
        tvCto.setText("0");
        tvCash.setText(getString(R.string.form_default_percent_cash));


        //add click listener
        ivStart.setOnClickListener(this);
        ivEnd.setOnClickListener(this);
        ivDate.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        spnNewCall.setOnItemSelectedListener(this);
        spnStatus.setOnItemSelectedListener(this);
        spnOtType.setOnItemSelectedListener(this);
        seekCashCto.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ctoCashValue = 100-progress;
                tvCash.setText(Integer.toString(ctoCashValue));
                tvCto.setText(Integer.toString(progress));
                Log.d(TAG, "onProgressChanged: cash=" + Integer.toString(ctoCashValue) + " cto=" + Integer.toString(progress) );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setDate(){
        FragmentManager fm = getFragmentManager();
        OvertimeDatePicker dialogFragment = new OvertimeDatePicker ();
        dialogFragment.setOnDateSetListener(new OvertimeDatePicker.OnDateSetListener() {
            @Override
            public void onDateSet(Date date) {
                etDate.setText(OvertimeDateFormatter.dateFormatForUi(date));
            }
        });
        dialogFragment.show(fm, "DatePicker");
    }


    private void setTime(final View view){
        LinearTimePickerDialog dialog = LinearTimePickerDialog.Builder.with(getActivity())
                .setButtonCallback(new LinearTimePickerDialog.ButtonCallback() {
                    @Override
                    public void onPositive(DialogInterface dialog, int hour, int minutes) {
                        ((TextView)view).setText(OvertimeDateFormatter.getDisplayTime(hour,minutes));
                    }
                    @Override
                    public void onNegative(DialogInterface dialog) {}
                })
                .build();
        dialog.show();
    }

    private void send(){

        if(validateForm()){
            Log.d(TAG, "send: VALID FORM");
            String eventNum = etEventNumber.getText().toString();
            Date eventDate = OvertimeDateFormatter.dateFromUiString(etDate.getText().toString());
            String reason = etReason.getText().toString();
            String shift = etShift.getText().toString();
            String sTime = etStartTime.getText().toString().replace(":","");
            String eTime = etEndTime.getText().toString().replace(":","");
            String comments = etComments.getText().toString();
            String empNum = getString(R.string.temp_user_phsa);
            String percentCash = Integer.toString(ctoCashValue);
            assert eventDate != null;

            Date s = OvertimeDateFormatter.getDateTime(eventDate,sTime);
            Date e = OvertimeDateFormatter.getDateTime(eventDate,eTime);
            OvertimeItem oti = new OvertimeItem(empNum,eventNum,eventDate.getTime(),shift,s.getTime(),e.getTime(),reason,mOvertimeType,isNewCall,percentCash,comments);
            OvertimeSend ots = new OvertimeSend(getActivity());
            ots.send(oti);
        }
    }

    private boolean validateForm(){
        ArrayList<Valid> errors = new ArrayList<>();
        Valid v;
        OvertimeAddValidator validator = new OvertimeAddValidator(getActivity());

        String eventNum = etEventNumber.getText().toString();
        v = validator.isValidEventNumber(eventNum);
        if(!v.isValid){
            etEventNumber.setError(v.error);
            errors.add(v);
        }

        v = validator.isValidDate(OvertimeDateFormatter.dateFromUiString(etDate.getText().toString()));
        if(!v.isValid){
            etDate.setError(v.error);
            errors.add(v);
        }

        v = validator.isValidReason(etReason.getText().toString());
        if(!v.isValid){
            etReason.setError(v.error);
            errors.add(v);
        }
        v = validator.isValidShift(etShift.getText().toString());
        if(!v.isValid){
            etShift.setError(v.error);
            errors.add(v);
        }

        v = validator.isValidTime(etStartTime.getText().toString());
        if(!v.isValid){
            etStartTime.setError(v.error);
            errors.add(v);
        }

        v = validator.isValidTime(etEndTime.getText().toString());
        if(!v.isValid){
            etEndTime.setError(v.error);
            errors.add(v);
        }

        return errors.size() == 0;
    }

    private void cancel(){
        getActivity().getFragmentManager().popBackStack();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_start_time:
                setTime(etStartTime);
                break;
            case R.id.iv_end_time:
                setTime(etEndTime);
                break;
            case R.id.btn_send:
                send();
                break;
            case R.id.btn_cancel:
                cancel();
                break;
            case R.id.iv_date_icon:
                setDate();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()){
            case R.id.spn_status:
                mEmpStatus = parent.getItemAtPosition(position).toString();
                Log.d(TAG, "onItemSelected: empStatus = " + mEmpStatus);
                break;
            case R.id.spn_new_call:
                isNewCall = parent.getItemAtPosition(position).toString();
                Log.d(TAG, "onItemSelected: NewCall = " + isNewCall);
                break;
            case R.id.spn_ot_type:
                mOvertimeType = parent.getItemAtPosition(position).toString();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
