package ca.datavic.overtime.frags.display;


import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ca.datavic.overtime.R;
import ca.datavic.overtime.database.OvertimeItem;
import ca.datavic.overtime.frags.add.FragAddOvertime;

import static ca.datavic.overtime.MainActivity.db;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragContentMain extends Fragment {
    FloatingActionButton mFloatingAddOt;
    View mView;
    ArrayList<OvertimeItem> mOtis = new ArrayList<>();
    public PayPeriodAdapter adapter;
    RecyclerView rvOtis;


    public FragContentMain() {
        // Required empty public constructor
    }

    public static FragContentMain newInstance() {
        return new FragContentMain();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_content_main, container, false);
        rvOtis = mView.findViewById(R.id.rv_ot_items);
        rvOtis.setLayoutManager(new LinearLayoutManager(mView.getContext()));
        rvOtis.setAdapter(adapter);

        new LoadDataTask().execute();

        //mAdapter = new OvertimeAdapter(mOtis);
        //rvOtis.setAdapter(mAdapter);


        mFloatingAddOt = mView.findViewById(R.id.floating_add_overtime);
        mFloatingAddOt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddOvertime();
            }
        });

        return mView;
    }


    private void showAddOvertime(){
        FragmentManager fragmentManager = getFragmentManager();
        FragAddOvertime addOvertime = new FragAddOvertime();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container_main, addOvertime,"OT");
        fragmentTransaction.addToBackStack("OT");
        fragmentTransaction.commit();
    }

    private void updateRv(){
        rvOtis.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @SuppressLint("StaticFieldLeak")
    class LoadDataTask extends AsyncTask<Object, Integer, String> {

        @Override
        protected String doInBackground(Object... objects) {
            adapter = new PayPeriodAdapter(DataFactory.makePayPeriods());
            return null;
        }
        @Override
        protected void onProgressUpdate(Integer... progress) {

        }
        @Override
        protected void onPostExecute(String result) {
            updateRv();
        }
    }
}
