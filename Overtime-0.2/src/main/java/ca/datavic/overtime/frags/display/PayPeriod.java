package ca.datavic.overtime.frags.display;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

import ca.datavic.overtime.database.OvertimeItem;

public class PayPeriod extends ExpandableGroup<OvertimeItem> {

    public PayPeriod(String title, ArrayList<OvertimeItem> items) {
        super(title, items);
    }


    @Override
    public boolean equals(Object o) {
        return this == o;
    }

}
