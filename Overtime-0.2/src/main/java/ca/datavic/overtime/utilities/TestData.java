package ca.datavic.overtime.utilities;

import android.content.Context;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

import ca.datavic.overtime.database.OvertimeDatabase;
import ca.datavic.overtime.database.OvertimeItem;

import static ca.datavic.overtime.MainActivity.db;

public class TestData {
    private static String TAG = "LOGGER:" + "TestData";
    public static void fillOti(Context context) {
        Log.d(TAG, "fillOti: loading data");


        Calendar cal = Calendar.getInstance();


        Date d1 = OvertimeDateFormatter.dateFromUiString("06 Jan 2018");
        cal.setTime(d1);
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 0);
        Date d1s = cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 15);
        Date d1e = cal.getTime();
        final OvertimeItem a = new OvertimeItem("345266",
                "11111", cal.getTimeInMillis(), "248D3",
                d1s.getTime(), d1e.getTime(),
                "Late Call", "Shift O/T", "Yes", "100", "");


        Date d2 = OvertimeDateFormatter.dateFromUiString("07 Jan 2018");
        cal.setTime(d2);
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 0);
        Date d2s = cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 15);
        Date d2e = cal.getTime();
        final OvertimeItem b = new OvertimeItem("345266",
                "11112", d2.getTime(), "248D3",
                d2s.getTime(), d2e.getTime(),
                "Late Call", "Extended Tour", "Yes", "100", "");


        Date d3 = OvertimeDateFormatter.dateFromUiString("08 Jan 2018");
        cal.setTime(d3);
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 0);
        Date d3s = cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 15);
        Date d3e = cal.getTime();
        final OvertimeItem c = new OvertimeItem("345266",
                "11113", d3.getTime(), "248D3",
                d3s.getTime(), d3e.getTime(),
                "Late Call", "Shift O/T", "Yes", "100", "");


        Date d4 = OvertimeDateFormatter.dateFromUiString("23 Jan 2018");
        cal.setTime(d3);
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 0);
        Date d4s = cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 15);
        Date d4e = cal.getTime();
        final OvertimeItem d = new OvertimeItem("345266",
                "11114", d4.getTime(), "248D3",
                d4s.getTime(), d4e.getTime(),
                "Late Call", "Shift O/T", "Yes", "100", "");

        Date d5 = OvertimeDateFormatter.dateFromUiString("24 Jan 2018");
        cal.setTime(d3);
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 0);
        Date d5s = cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 15);
        Date d5e = cal.getTime();
        final OvertimeItem e = new OvertimeItem("345266",
                "11115", d5.getTime(), "248D3",
                d5s.getTime(), d5e.getTime(),
                "Late Call", "Shift O/T", "Yes", "100", "");


        new Thread(new Runnable() {
            @Override
            public void run() {
                db.otiDao().deleteAll();
                db.otiDao().insert(a);
                db.otiDao().insert(b);
                db.otiDao().insert(c);
                db.otiDao().insert(d);
                db.otiDao().insert(e);
                Log.d(TAG, "run: size = " + db.otiDao().getAllItems().size());
            }
        }).start();

    }
}
