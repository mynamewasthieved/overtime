package ca.datavic.overtime.utilities;

import android.app.Activity;
import android.util.Log;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ca.datavic.overtime.R;

import static ca.datavic.overtime.MainActivity.db;

public class OvertimeAddValidator {
    private static String TAG = "LOGGER:" + "OvertimeAddValidator";
    Activity mActivity;

    public static class Valid{
        public boolean isValid = true;
        public String error = "";
    }

    public OvertimeAddValidator(Activity activity){
        this.mActivity = activity;
    }

    public Valid isValidTime(String time){
        Valid v = new Valid();
        if(!time.matches("[0-9][0-9]:[0-9][0-9]")){
            v.isValid = false;
            v.error = mActivity.getString(R.string.error_invalid_time);
        }
        return v;
    }

    public Valid isValidShift(String shift){
        Valid v = new Valid();
        if(shift.isEmpty()){
           v.isValid = false;
           v.error = mActivity.getString(R.string.error_empty);
        } else {
            if(!shift.matches("[1-9][0-9][0-9][a-zA-Z][1-9]")){
                Log.d(TAG, "isValidShift: NO MATCH for " + shift );
                v.isValid = false;
                v.error = mActivity.getString(R.string.error_invalid_shift);
            }
        }
        return v;
    }

    public Valid isValidReason(String reason){
        Valid v = new Valid();
        if(reason.isEmpty()){
            v.isValid = false;
            v.error = mActivity.getString(R.string.error_empty);
        }
        return v;
    }

    public Valid isValidDate(Date date){
        final Valid v = new Valid();

        if(date == null){
            v.isValid = false;
            v.error = mActivity.getString(R.string.error_invalid_date);
        } else {
            v.isValid = true;
        }
        return v;
    }

    public Valid isValidEventNumber(final String eventNumber){
        final Valid v = new Valid();
        if(eventNumber.isEmpty()){
            v.isValid = false;
            v.error = mActivity.getString(R.string.error_empty);
        } else {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Callable<Boolean> callable = new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    if(db.otiDao().getItemsByEvent(eventNumber) == null){
                        Log.d(TAG, "call: event not found in database");
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            Future<Boolean> future = executor.submit(callable);
            executor.shutdown();
            try {
                if(future.get()){
                    return v;
                } else {
                    v.isValid = false;
                    v.error = mActivity.getString(R.string.error_event_number_in_db);
                }
            } catch (InterruptedException e) {
                v.isValid = false;
                v.error = "Database Error (InterruptedException) ... well shit";
                e.printStackTrace();
            } catch (ExecutionException e) {
                v.isValid = false;
                v.error = "Database Error (ExecutionException) ... well shit";
                e.printStackTrace();
            }
        }
        return v;
    }

}
