package ca.datavic.overtime.utilities;

import android.content.Context;
import android.content.res.AssetManager;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Objects;

import ca.datavic.overtime.R;


public class OvertimeFileHandler {
    public enum FileLocation {ASSETS,EXTERNAL}
    private Context mContext;
    private String mSrcFileName,mDesFileName;
    private InputStream input = null;
    private OutputStream output = null;

    public OvertimeFileHandler(Context context){
        mContext = context;
        mSrcFileName = mContext.getString(R.string.excel_template_file_name);
        mDesFileName = OvertimeDateFormatter.dateFormatForFileName(new Date()) +
                mContext.getString(R.string.excel_template_file_name);
    }

    public File copy() {
        try {
            input = getInputStream(FileLocation.EXTERNAL,File.separator+ "template" + File.separator +mSrcFileName);
            output = getOutputStream(mDesFileName);
            IOUtils.copy(input,output);
            input.close();
            output.close();
            return getFile(mContext,mDesFileName);
        } catch (IOException e) {
            filesSetup();
            copy();
            e.printStackTrace();
            return null;
        }
    }

    public boolean filesSetup(){
        String folder_template = "template";

        File templateDir = new File(mContext.getExternalFilesDir(null), folder_template);
        if (!templateDir.exists()) {
            //noinspection ResultOfMethodCallIgnored
            templateDir.mkdirs();
        }

        File out = new File(Objects.requireNonNull(mContext.getExternalFilesDir(null))
                .getAbsolutePath(), folder_template + File.separator + mSrcFileName);
        if (!out.exists()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                out.createNewFile();
                input = getInputStream(FileLocation.ASSETS,mSrcFileName);
                output = new FileOutputStream(out);
                IOUtils.copy(input,output);
                input.close();
                output.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return true;
        }
    }

    private static File getFile(Context context, String fileName){
        return new File(Objects.requireNonNull(context.getExternalFilesDir(null))
                .getAbsolutePath(), fileName);
    }

    private InputStream getInputStream(FileLocation location, String fileName) throws IOException{
        switch (location){
            case ASSETS:
                AssetManager assetManager = mContext.getAssets();
                return assetManager.open(fileName);
            case EXTERNAL:
                return new FileInputStream(getFile(mContext,fileName));
            default: return null;
        }
    }

    private OutputStream getOutputStream(String fileName)throws IOException {
        return new FileOutputStream(getFile(mContext,fileName));
    }


}
