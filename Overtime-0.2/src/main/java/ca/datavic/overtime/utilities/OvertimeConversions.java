package ca.datavic.overtime.utilities;

import java.math.BigDecimal;

public class OvertimeConversions {

    private static double round(double value, int decimalPlace) {
        long factor = (long) Math.pow(10, decimalPlace);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private static double millisecondsToBase10(long milliseconds){
        return (milliseconds/3600000.00);
    }

    public static double durationAsRoundedDecimal(long startTime, long endTime){
        long duration = endTime - startTime;
        double durationAsDecimal = millisecondsToBase10(duration);
        return round(durationAsDecimal,2);
    }
}
