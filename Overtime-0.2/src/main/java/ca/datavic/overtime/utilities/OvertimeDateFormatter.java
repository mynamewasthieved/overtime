package ca.datavic.overtime.utilities;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class OvertimeDateFormatter {
    private static String TAG = "LOGGER:" + "OvertimeDateFormatter";

    public static String dateFormatForFileName(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy_", Locale.CANADA);
        return sdf.format(date);
    }
    public static String dateFormatForUi(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.CANADA);
        return sdf.format(date);
    }

    public static String dateFormatForExcel(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd", Locale.CANADA);
        return sdf.format(date);
    }

    public static String timeFormatForExcel(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm", Locale.CANADA);
        return sdf.format(date);
    }

    public static Date dateFromUiString(String date){
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd MMMM yyyy",Locale.CANADA);
        try {
            return simpledateformat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String dateTimeForLog(Date date){
        SimpleDateFormat logFormat = new SimpleDateFormat("yyyy/MMM/dd HH:mm");
        return logFormat.format(new Date(date.getTime()));

    }

    public static String timeFromLong(long time){
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        return timeFormat.format(time);
    }

    public static String getPayPeriodTitle(Date start,Date end){
        return dateFormatForUi(start) + " <--> " + dateFormatForUi(end);
    }

    public static String getDisplayTime(int hour,int min){
        String time = "";
        if(hour < 10){
            time = time.concat("0" + String.valueOf(hour));
        } else {
            time = String.valueOf(hour);
        }
        time = time.concat(":");
        if(min < 10){
            time = time.concat("0" + String.valueOf(min));
        } else {
            time = time.concat(String.valueOf(min));
        }
        return time;
    }

    public static Date getDateTime(Date date, String time){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        Log.d(TAG, "getDateTime: time = " + time);
        Log.d(TAG, "getDateTime: time (H) = " + time.substring(0,2));
        Log.d(TAG, "getDateTime: time (m) = " + time.substring(2,4));

        int hour = Integer.parseInt(time.substring(0,2));
        int minutes = Integer.parseInt(time.substring(2,4));

        Log.d(TAG, "getDateTime: h="+ String.valueOf(hour) + "m=" + String.valueOf(minutes));

        cal.set(Calendar.HOUR_OF_DAY,hour);
        cal.set(Calendar.MINUTE,minutes);

        Date result = new Date(cal.getTimeInMillis());

        return result;
    }



}
